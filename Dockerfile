# # FROM golang:1.14

# # #EXPOSE 8080

# # #WORKDIR /go/src/app
# # # COPY . .


# # #'RUN apk add build-base
# # #WORKDIR /go/src/app/src

# # RUN go get -d -v ./
# # #RUN go build -v ./

# # RUN go install -v ./

# # CMD ["src"]

# FROM golang

# # if left blank app will run with dev settings
# # to build production image run:
# # $ docker build ./api --build-args app_env=production
# ARG app_env
# ENV APP_ENV $app_env

# # it is okay to leave user/GoDoRP as long as you do not want to share code with other libraries
# COPY . /go/src/gitlab.com/johnfg10/userservice
# WORKDIR /go/src/gitlab.com/johnfg10/userservice/src


# RUN go get ./
# RUN go build

# # if dev setting will use pilu/fresh for code reloading via docker-compose volume sharing with local machine
# # if production setting will build binary
# CMD if [ ${APP_ENV} = production ]; \
#     then \
#     api; \
#     else \
#     go get github.com/pilu/fresh && \
#     fresh; \
#     fi

# EXPOSE 8080
FROM golang

WORKDIR /go/src/gitlab.com/johnfg10/policyservice

CMD ["go", "run", "src/main.go"]

EXPOSE 8080