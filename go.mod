module gitlab.com/cabzy/policyservice.git

go 1.14

require (
	github.com/casbin/casbin v1.9.1
	github.com/casbin/casbin-pg-adapter v0.1.4-0.20200217145850-13bf718c9a19
	github.com/casbin/casbin/v2 v2.2.2 // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/nats-io/jwt v0.3.2
	github.com/rs/zerolog v1.18.0
	github.com/spf13/viper v1.6.3
	gitlab.com/cabzy/x.git v0.0.0-20200501081922-1e9d21eba62f
	go.uber.org/fx v1.12.0
	gopkg.in/square/go-jose.v2 v2.3.1
)
