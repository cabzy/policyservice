package api

import (
	"github.com/rs/zerolog"
	"gopkg.in/square/go-jose.v2/jwt"

	"github.com/casbin/casbin"
	"github.com/gin-gonic/gin"
	"gitlab.com/cabzy/policyservice/pkg/models"
	"gitlab.com/cabzy/x.git/genericresponses"
	"gitlab.com/cabzy/x.git/jwthandler"
	"go.uber.org/fx"
)

var EnforcerAPIModule = fx.Provide(NewEnforcerAPI)

type EnforcerAPI struct {
	enforcer *casbin.Enforcer
	logger   *zerolog.Logger
}

func NewEnforcerAPI(enforcer *casbin.Enforcer, logger *zerolog.Logger) *EnforcerAPI {
	return &EnforcerAPI{
		enforcer: enforcer,
		logger:   logger,
	}
}

func (e *EnforcerAPI) GetCheckPolicy(c *gin.Context) {
	var form models.PolicyForm
	err := c.BindQuery(&form)
	if err != nil {
		genericresponses.BadRequest(c)
		return
	}
	if form.IsValid() {
		genericresponses.BadRequest(c)
		return
	}

	genericresponses.OK(c, e.enforcer.Enforce(form.Subject, form.Tenant, form.Resource, form.Action))
	// subject := c.Query("sub")
	// tenant := c.Query("ten")
	// resource := c.Query("res")
	// action := c.Query("act")

	// if subject == "" || tenant == "" || resource == "" || action == "" {
	// 	genericresponses.BadRequest(c)
	// 	return
	// }

	//allowed := e.enforcer.Enforce(subject, tenant, resource, action)
	// if e.enforcer.Enforce(form.Subject, form.Tenant, form.Resource, form.Action) {
	// 	genericresponses.OK(c, true)
	// } else {
	// 	genericresponses.OK(c, false)
	// }
}

func (e *EnforcerAPI) PostCreatePolicy(c *gin.Context) {
	val, _ := c.Get(jwthandler.CtxUserClaims)
	claim := val.(*jwt.Claims)

	var form models.PolicyForm
	err := c.BindJSON(&form)
	if err != nil {
		e.logger.Error().Err(err).Msg("Create policy failed when binding json")
		genericresponses.BadRequest(c)
		return
	}

	if e.enforcer.Enforce(claim.Subject, form.Tenant, "policy", "create") {
		//allowed
		e.enforcer.AddPolicy(form.Subject, form.Tenant, form.Resource, form.Action)
		e.enforcer.SavePolicy()
		genericresponses.OK(c, nil)
		return
	}

	c.Status(401)
}

func (e *EnforcerAPI) DeletePolicy(c *gin.Context) {
	val, _ := c.Get(jwthandler.CtxUserClaims)
	claim := val.(*jwt.Claims)

	var form models.PolicyForm
	err := c.BindJSON(&form)
	if err != nil {
		e.logger.Error().Err(err).Msg("Delete policy failed when binding json")
		genericresponses.BadRequest(c)
		return
	}

	if e.enforcer.Enforce(claim.Subject, form.Tenant, "policy", "delete") {
		//allowed
		e.enforcer.RemovePolicy(form.Subject, form.Tenant, form.Resource, form.Action)
		e.enforcer.SavePolicy()
		genericresponses.OK(c, nil)
		return
	}

	c.Status(401)
}
