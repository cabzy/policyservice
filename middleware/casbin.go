package middleware

import (
	"github.com/casbin/casbin"
	pgadapter "github.com/casbin/casbin-pg-adapter"
	"go.uber.org/fx"
)

var CasbinModule = fx.Provide(NewAdapater)

func NewAdapater(adapter *pgadapter.Adapter) (*casbin.Enforcer, error) {
	enforcer := casbin.NewEnforcer("policys/abac_model.conf", "policys.csv")
	err := enforcer.LoadPolicy()
	//err := enforcer.LoadPolicy()
	return enforcer, err
}
