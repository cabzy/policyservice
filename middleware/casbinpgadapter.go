package middleware

import (
	"fmt"
	"reflect"

	"github.com/spf13/viper"

	pgadapter "github.com/casbin/casbin-pg-adapter"
	"github.com/go-pg/pg"
	"go.uber.org/fx"
)

var CasbinPgAdapterModule = fx.Provide(NewCasbinPgAdapater)

func NewCasbinPgAdapater(opts *pg.Options, v *viper.Viper) (*pgadapter.Adapter, error) {
	fmt.Println(reflect.TypeOf(opts))
	return pgadapter.NewAdapter(v.GetString("database.url"))
}
