package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/cabzy/policyservice.git/pkg/models"
	"go.uber.org/fx"

	"gitlab.com/cabzy/x.git/genericresponses"
)

var Module = fx.Provide(New)

//PolicyClient
type PolicyClient struct {
	URL string
}

func New(url string) *PolicyClient {
	return &PolicyClient{
		URL: url,
	}
}

func (p *PolicyClient) CheckPolicy(form *models.PolicyForm) (bool, error) {
	resp, err := http.Get(p.URL + "/policy" + "?" + "subject=" + form.Subject + "&tenant=" + form.Tenant + "&resource=" + form.Resource + "&action=" + form.Action)
	if err != nil {
		return false, err
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	var respJSON genericresponses.GenericResponse
	err = json.Unmarshal(bytes, &respJSON)
	if err != nil {
		return false, err
	}
	fmt.Println(respJSON.Payload.(bool))

	return respJSON.Payload.(bool), nil
}

func (p *PolicyClient) CreatePolicy(form *models.PolicyForm) error {
	bys, err := json.Marshal(form)
	if err != nil {
		return err
	}
	resp, err := http.Post(p.URL+"/policy/post", "application/json", bytes.NewBuffer(bys))
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Policy Client: Create Policy: Invalid status code %d ", resp.StatusCode)
	}

	return nil
}

func (p *PolicyClient) DeletePolicy(form *models.PolicyForm) error {
	bys, err := json.Marshal(form)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("DELETE", p.URL+"/policy", bytes.NewBuffer(bys))
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Policy Client: Create Policy: Invalid status code %d ", resp.StatusCode)
	}

	return nil
}
