package models

type PolicyForm struct {
	Subject  string `json:"subject"`
	Tenant   string `json:"tenant"`
	Resource string `json:"resource"`
	Action   string `json:"action"`
}

func (f *PolicyForm) IsValid() bool {
	return !(f.Subject == "" || f.Tenant == "" || f.Resource == "" || f.Action == "")
}
