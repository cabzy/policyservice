package main

import (
	"context"

	"gitlab.com/cabzy/policyservice.git/internal/api"
	"gitlab.com/cabzy/policyservice.git/middleware"
	"gitlab.com/cabzy/x.git/sonyflakehandler"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"gitlab.com/cabzy/x.git/ginzerolog"
	"gitlab.com/cabzy/x.git/jwthandler"
	"gitlab.com/cabzy/x.git/loghandler"
	"gitlab.com/cabzy/x.git/pgzerolog"
	"gitlab.com/cabzy/x.git/postgreshandler"
	"gitlab.com/cabzy/x.git/postgresoptionshandler"
	"gitlab.com/cabzy/x.git/requestidhandler"
	"gitlab.com/cabzy/x.git/viperhandler"
	"go.uber.org/fx"
)

func main() {
	fx.New(
		sonyflakehandler.Module,
		requestidhandler.Module,
		loghandler.Module,
		viperhandler.Module,
		postgresoptionshandler.Module,
		postgreshandler.Module,
		fx.Provide(func(v *viper.Viper) *client.PolicyClient {
			return client.New(v.GetString("policy.client.url"))
		}),
		pgzerolog.Module,
		jwthandler.Module,
		middleware.CasbinPgAdapterModule,
		middleware.CasbinModule,
		api.EnforcerAPIModule,
		fx.Invoke(Invoke),
	).Run()
}

func Invoke(lifecycle fx.Lifecycle, logger *zerolog.Logger, db *pg.DB, jwtHandler *jwthandler.JWTHandler, requestIDHandler *requestidhandler.RequestIDHandler, api *api.EnforcerAPI) {
	router := gin.New()

	router.Use(gin.Recovery())
	router.Use(cors.Default())
	router.Use(requestIDHandler.Handler())
	router.Use(ginzerolog.Logger("userservice", logger))
	//router.GET("/business", businessAPI.GetClient)
	router.GET("/policy", api.GetCheckPolicy)
	secureGroup := router.Group("/")
	secureGroup.Use(jwtHandler.Handler())
	secureGroup.POST("/policy/form", api.PostCreatePolicy)
	secureGroup.DELETE("/policy", api.DeletePolicy)

	lifecycle.Append(
		fx.Hook{
			OnStart: func(ctx context.Context) error {
				runRouter := func() {
					router.Run(":8080")
				}
				go runRouter()
				return nil
			},
			OnStop: func(ctx context.Context) error {
				return nil
			},
		},
	)

}
